
window.addEventListener('DOMContentLoaded', () => {
    document.querySelector(".cost__form__button").addEventListener('click', (event)=>{
        document.querySelector('.cost__answer').classList.toggle("cost__answer_visible");
    })

    const questions = document.querySelectorAll(".qa-question");
    console.log(questions[0].lastChild);
    questions.forEach(question =>{
        question.addEventListener('click', (event)=>{
            var mainPart = event.target;
            if (!event.target.matches('.qa-question__main_part')){
                mainPart = event.target.closest('.qa-question__main_part');
            }
            console.log(mainPart);
            console.log("dffgfg");
            mainPart.parentNode.querySelector(".qa-question_answer").classList.toggle('qa-question_answer_visible');
            // mainPart.parentNode.querySelector(".qa-question_answer").classList.remove('animation_hiding');
            // mainPart.parentNode.querySelector(".qa-question_answer").classList.toggle('animation_appearing');
            mainPart.querySelector(".qa-question_button").textContent = mainPart.querySelector(".qa-question_button").textContent == "×"? "+": "×";
            console.log(mainPart.querySelector(".qa-question_button").textContent);
            for (var i = 0; i<questions.length;i++){
                if (questions[i].querySelector(".qa-question__main_part") != mainPart){
                    questions[i].querySelector(".qa-question_answer").classList.remove('qa-question_answer_visible');
                    // questions[i].querySelector(".qa-question_answer").classList.remove('qa-animation_appearing');
                    // questions[i].querySelector(".qa-question_answer").classList.add('animation_hiding');
                    questions[i].querySelector(".qa-question_button").textContent = "+";
                }
            }
        })
    })

    const worksBtn = document.querySelectorAll('.works__button');
    console.log(worksBtn);
    worksBtn.forEach(button =>{
        button.addEventListener('click', (event)=>{
            console.log(event.target);
            event.target.classList.toggle('works__button_selected');
            for (var i=0; i < worksBtn.length;i++){
                if (worksBtn[i] != event.target && worksBtn[i].classList.contains('works__button_selected')){
                    worksBtn[i].classList.toggle('works__button_selected');
                }
            }
            if (event.target.id == "works-key"){
                console.log(document.querySelector("#new-collection"));
                document.querySelector("#new-collection").style.display = "none";
                document.querySelector("#neon-ws").className = "works__img_big";
            }
            else{
                const worksImgs = document.querySelectorAll(".works__examples img");
                console.log(worksImgs);
                worksImgs.forEach(img=> img.style.display = "block");
                document.querySelector("#neon-ws").className = "works__img_small";
            }
        });
    })
    
})